crypto = require 'crypto'
Redsys = require('node-redsys-api').Redsys
redsys = new Redsys();

_ = require 'lodash'

Utils = require './utils'

currency_mapping =
  'EUR': 978
  'USD': 840
  'GB': 826

language_mapping =
  'auto': 0
  'es': 1
  'en': 2

class Redsys
  @TransactionTypes:
    STANDAR_PAYMENT: '0'
    CARD_IN_ARCHIVE_INITIAL: 'L'
    CARD_IN_ARCHIVE: 'M'

  constructor: (@config = {}) ->
    # Credit card for tests 
    # 4548 8120 4940 0004
    # 12/17
    # CVV2: 123
    # CIP: 123456
    @form_url = if @config.test then "https://sis-t.redsys.es:25443/sis/" else "https://sis.redsys.es/sis/"
    @merchant_code = if @config.test then "327234688" else @config.merchant.code 
    @merchant_secret = if @config.test then "sq7HjrUOBfKmC576ILgskD5srU870gJ7" else @config.merchant.secret 
      
    @config.language = @convert_language(@config.language)

  build_payload: (data) ->
    str = "" +
      data.total +
      data.order +
      @config.merchant.code +
      data.currency

    str += data.transaction_type if typeof(data.transaction_type) != 'undefined'
    str += data.redirect_urls?.callback if data.redirect_urls?.callback

    str += @config.merchant.secret

    str

  build_response_payload: (data) ->
    str = "" +
      data.Ds_Amount +
      data.Ds_Order +
      data.Ds_MerchantCode +
      data.Ds_Currency +
      data.Ds_Response +
      @config.merchant.secret

    str

  sign: (data) =>
    #shasum = crypto.createHash 'sha1'
    shasum = crypto.createHash 'sha256'
    shasum.update data
    console.log typeof shasum
    shasum.digest 'hex'

  convert_currency: (currency) ->
    console.log currency + ' - ' + currency_mapping[currency]
    currency_mapping[currency] || "Unknown!"

  convert_language: (language) ->
    if typeof language_mapping[language] is 'undefined'
      "Unknown!"
    else
      language_mapping[language]

  normalize: (data) ->
    if Math.floor(data.total) isnt data.total
      data.total *= 100

    data.currency = @convert_currency(data.currency)

    normalize_data =
      total: data.total
      currency: data.currency
      description: Utils.format data.description, 125
      titular: Utils.format @config.merchant.titular, 60
      merchant_code: Utils.formatNumber @merchant_code, 9
      merchant_url: Utils.format data.redirect_urls?.callback, 250
      merchant_url_ok: Utils.format data.redirect_urls?.return_url, 250
      merchant_url_ko: Utils.format data.redirect_urls?.cancel_url, 250
      merchant_name: Utils.format @config.merchant.name, 25
      language: Utils.formatNumber @config.language, 3
      terminal: Utils.formatNumber @config.merchant.terminal, 3
      transaction_type: data.transaction_type

    if data.transaction_type is "L"
      data.order = Utils.format data.order, 4, 10
      normalize_data.order = data.order
    else
      data.order = Utils.format data.order, 4, 12
      normalize_data.order = data.order

    normalize_data.authorization_code = Utils.formatNumber data.authorization_code, 6 if data.authorization_code
    normalize_data.data = Utils.format data.data, 1024 if data.data

    normalize_data.signature = Utils.format @sign(@build_payload data), 40


    normalize_data

  encrypt3DES: (str,key) ->
    secretKey = new Buffer(key, 'base64')
    iv = new Buffer(8)
    iv.fill(0)
    cipher = crypto.createCipheriv('des-ede3-cbc', secretKey, iv)
    cipher.setAutoPadding(false)
    res=cipher.update(zeroPad(str, 8), 'utf8', 'base64') + cipher.final('base64')
    res

  create_payment: (order_data) =>
    tpv_data = @normalize(order_data)
    console.log tpv_data
    #* quitar signature de tpv_data

    former_form_data =
      DS_MERCHANT_AMOUNT: tpv_data.total.toString()
      DS_MERCHANT_CURRENCY: tpv_data.currency.toString()
      DS_MERCHANT_ORDER: tpv_data.order
      DS_MERCHANT_MERCHANTCODE: @merchant_code
      DS_MERCHANT_CONSUMERLANGUAGE: tpv_data.language
      DS_MERCHANT_TERMINAL: tpv_data.terminal
      DS_MERCHANT_TRANSACTIONTYPE: tpv_data.transaction_type
      DS_MERCHANT_MERCHANTURL: tpv_data.merchant_url
      DS_MERCHANT_URLOK: tpv_data.merchant_url_ok
      DS_MERCHANT_URLKO: tpv_data.merchant_url_ko
    

    merchant_params = redsys.createMerchantParameters(former_form_data); 
    former_form_signature = redsys.createMerchantSignature(@merchant_secret,former_form_data)

    console.log "former_form_signature: #{former_form_signature}"
    console.log "@merchant_secret: #{@merchant_secret}"
    console.log "@merchant_code: #{@merchant_code}"

    new_form_data =
      URL: @form_url + "realizarPago"
      Ds_SignatureVersion: 'HMAC_SHA256_V1'
      Ds_MerchantParameters: merchant_params
      Ds_Signature: former_form_signature

    if tpv_data.transaction_type isnt "L"
      _.extend form_data,
        Ds_Merchant_Titular: tpv_data.titular
        Ds_Merchant_ProductDescription: tpv_data.description
        Ds_Merchant_MerchantName: tpv_data.merchant_name

      new_form_data.Ds_Merchant_AuthorisationCode = tpv_data.authorization_code if tpv_data.authorization_code
      new_form_data.Ds_Merchant_MerchantData = tpv_data.data if tpv_data.data

    console.log "tpv_data.order #{tpv_data.order}"
    console.log "new_form_data #{new_form_data}"
    console.log "new_form_data: #{JSON.stringify(new_form_data)}"
    new_form_data
    
  validate_response: (response) =>
    signature = @sign(@build_response_payload response)
    response.Ds_Signature.toLowerCase() is signature

module.exports =
  Redsys: Redsys
